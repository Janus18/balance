
using Balance.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Balance.Middleware;

public static class InvalidStateResponse
{
    public static IActionResult GetResponse(ActionContext context)
    {
        var errors = GetErrors(context.ModelState);
        return new BadRequestObjectResult(errors);
    }

    private static IList<Error> GetErrors(this ModelStateDictionary model)
    {
        return model.SelectMany(entry =>
            entry.Value?.Errors.Select(err => new Error(entry.Key, err.ErrorMessage)) ?? new List<Error>()
        ).ToList();
    }
} 
