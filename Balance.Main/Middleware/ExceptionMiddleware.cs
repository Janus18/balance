
namespace Balance.Middleware;

public class ExceptionMiddleware {
    private readonly RequestDelegate next;

    public ExceptionMiddleware(RequestDelegate next)
    {
        this.next = next;
    }

    public async Task InvokeAsync(HttpContext context, ILogger<ExceptionMiddleware> logger) {
        try {
            await next.Invoke(context);
        }
        catch (Exception ex) {
            logger.LogError(ex, "Unexpected controller error");
        }
    }
}

public static class ExceptionMiddlewareExtensions {
    public static IApplicationBuilder UseExceptionMiddleware(this IApplicationBuilder app) {
        return app.UseMiddleware<ExceptionMiddleware>();
    }
}
