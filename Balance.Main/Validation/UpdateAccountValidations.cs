
using Balance.Domain.Dto;
using Balance.Resources;
using FluentValidation;
using Microsoft.Extensions.Localization;

namespace Balance.Validation;

public class UpdateAccountValidations : AbstractValidator<UpdateAccountDto> {
    public UpdateAccountValidations(IStringLocalizer<ValidationStrings> localizer)
    {
        RuleFor(x => x.Name)
            .Cascade(CascadeMode.Stop)
            .NotNull().WithMessage(localizer["AccountNameNotNull"])
            .NotEmpty().WithMessage(localizer["AccountNameNotNull"]);
    }
}
