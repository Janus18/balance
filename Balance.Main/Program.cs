using Balance;
using Balance.DataAccess;
using Balance.DataAccess.Impl;
using Balance.Middleware;
using Balance.Validation;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);
builder.Configuration.AddJsonFile("appsettings.json", false, true);
builder.Configuration.AddUserSecrets<Configuration>(false, true);

string defaultDbConnection = builder.Configuration.GetConnectionString("DatabaseConnection") ?? "";
builder.Services.AddDbContext<ApplicationDataContext>(builder =>
    builder.UseNpgsql(defaultDbConnection)
);

builder.Services.AddScoped<IRepository, Repository>();
builder.Services.AddMediatR(typeof(Balance.Commands.Handlers.CreateAccountHandler));
builder.Services.AddAutoMapper(typeof(Balance.Domain.Profiles.AccounDtoProfile).Assembly);
builder.Services.AddLocalization();
builder.Services.AddControllers()
    .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<CreateAccountValidations>());
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.Configure<ApiBehaviorOptions>(opts => {
    opts.InvalidModelStateResponseFactory = InvalidStateResponse.GetResponse;
});

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseRequestLocalization(new RequestLocalizationOptions {
    DefaultRequestCulture = new Microsoft.AspNetCore.Localization.RequestCulture("es")
});

app.UseHttpsRedirection();
app.UseAuthorization();
app.UseExceptionMiddleware();
app.MapControllers();
app.RunMigrations();
app.Run();

public partial class Program { }
