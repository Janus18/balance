
using Microsoft.EntityFrameworkCore.Storage;

namespace Balance.DataAccess.Impl;

public class Transaction : ITransaction
{
    private readonly IDbContextTransaction transaction;

    public Transaction(IDbContextTransaction transaction)
    {
        this.transaction = transaction;
    }

    public Task Commit()
    {
        return transaction.CommitAsync();
    }

    public Task Rollback()
    {
        return transaction.RollbackAsync();
    }

    public void Dispose()
    {
        transaction.Dispose();
    }
}
