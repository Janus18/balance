
using System.Linq.Expressions;
using Balance.DataAccess.Order;
using Microsoft.EntityFrameworkCore;

namespace Balance.DataAccess.Impl;

public class Repository : IRepository
{
    private readonly ApplicationDataContext context;

    public Repository(ApplicationDataContext context)
    {
        this.context = context;
    }

    public Task<T?> Find<T>(
        Expression<Func<T, bool>> where,
        IEnumerable<string>? include = null
    ) where T : class
    {
        IQueryable<T> query = context.Set<T>();
        if (include?.Any() == true)
        {
            foreach (var inc in include)
            {
                query = query.Include(inc);
            }
        }
        return query.FirstOrDefaultAsync(where);
    }

    public async Task<IEnumerable<T>> FindMany<T>(Expression<Func<T, bool>>? where = null, IOrder<T>? order = null) where T : class
    {
        IQueryable<T> query = context.Set<T>();
        if (where != null) {
            query = query.Where(where);
        }
        if (order != null) {
            query = order.Order(query);
        }
        return await query.ToListAsync();
    }

    public void Add<T>(T t) where T : class
    {
        context.Set<T>().Add(t);
    }

    public void AddMany<T>(IEnumerable<T> t) where T : class
    {
        context.Set<T>().AddRange(t);
    }

    public void Update<T>(T t) where T : class
    {
        context.Set<T>().Update(t);
    }

    public void Delete<T>(T t) where T : class
    {
        context.Set<T>().Remove(t);
    }

    public ITransaction StartTransaction()
    {
        return new Transaction(context.Database.BeginTransaction());
    }

    public Task RawSqlCommand(string command, params SqlParam[] parameters)
    {
        return context.Database.ExecuteSqlRawAsync(command, parameters.Select(p => p.Value));
    }

    public async Task Save() {
        await context.SaveChangesAsync();
    }

    private static string TableName<T>() {
        return nameof(T);
    }
}
