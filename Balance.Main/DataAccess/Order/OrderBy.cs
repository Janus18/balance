
using System.Linq.Expressions;

namespace Balance.DataAccess.Order;

public class OrderBy<T, T2> : BaseOrder<T, T2>
{
    public OrderBy(Expression<Func<T, T2>> exp, IOrder<T>? parent = null) : base(exp, parent)
    {
    }

    public override IOrderedQueryable<T> By(IQueryable<T> query)
    {
        return query.OrderBy(exp);
    }

    public override IOrderedQueryable<T> ThenBy(IOrderedQueryable<T> query)
    {
        return query.ThenBy(exp);
    }
}
