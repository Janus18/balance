
using System.Linq.Expressions;

namespace Balance.DataAccess.Order;

public static class Order<T>
{
    public static IOrder<T> By<T3>(Expression<Func<T, T3>> exp)
    {
        return new OrderBy<T, T3>(exp, null);
    }

    public static IOrder<T> ByDesc<T3>(Expression<Func<T, T3>> exp)
    {
        return new OrderByDescending<T, T3>(exp, null);
    }
}
