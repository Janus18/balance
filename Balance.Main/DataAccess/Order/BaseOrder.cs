
using System.Linq.Expressions;

namespace Balance.DataAccess.Order;

public abstract class BaseOrder<T, T2> : IOrder<T>, IOrderBy<T> {
    protected readonly Expression<Func<T, T2>> exp;

    protected readonly IOrder<T>? parent;

    public BaseOrder(Expression<Func<T, T2>> exp, IOrder<T>? parent = null)
    {
        this.exp = exp;
        this.parent = parent;
    }

    public IOrder<T> ThenBy<T3>(Expression<Func<T, T3>> exp) {
        return new OrderBy<T, T3>(exp, this);
    }

    public IOrder<T> ThenByDesc<T3>(Expression<Func<T, T3>> exp) {
        return new OrderByDescending<T, T3>(exp, this);
    }

    public IOrderedQueryable<T> Order(IQueryable<T> query)
    {
        return Order(query, new Stack<IOrderBy<T>>());
    }

    public abstract IOrderedQueryable<T> By(IQueryable<T> query);

    public abstract IOrderedQueryable<T> ThenBy(IOrderedQueryable<T> query);

    protected IOrderedQueryable<T> Order(IQueryable<T> query, Stack<IOrderBy<T>> stack) {
        if (parent is null) {
            IOrderedQueryable<T> q = By(query);
            while (stack.Count > 0) {
                q = stack.Pop().ThenBy(q);
            }
            return q;
        }
        stack.Push(this);
        return Order(query, stack);
    }
}
