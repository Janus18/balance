
namespace Balance.DataAccess.Order;

public interface IOrder<T> {
    IOrderedQueryable<T> Order(IQueryable<T> query);
}
