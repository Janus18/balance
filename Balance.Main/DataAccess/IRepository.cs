
using System.Linq.Expressions;
using Balance.DataAccess.Order;

namespace Balance.DataAccess;

public interface IRepository {
    Task<T?> Find<T>(
        Expression<Func<T, bool>> where,
        IEnumerable<string>? include = null
    ) where T : class;

    Task<IEnumerable<T>> FindMany<T>(
        Expression<Func<T, bool>>? where = null,
        IOrder<T>? order = null
    ) where T : class;

    void Add<T>(T t) where T : class;

    void AddMany<T>(IEnumerable<T> t) where T : class;

    void Update<T>(T t) where T : class;

    void Delete<T>(T t) where T : class;

    ITransaction StartTransaction();    

    Task RawSqlCommand(string command, params SqlParam[] parameters);

    Task Save();
}
