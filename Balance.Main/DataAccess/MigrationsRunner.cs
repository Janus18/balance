
using Microsoft.EntityFrameworkCore;

namespace Balance.DataAccess;

public static class MigrationsRunner {
    public static void RunMigrations(this IApplicationBuilder builder) {
        using var scope = builder.ApplicationServices.CreateScope();
        var dbContext = scope.ServiceProvider.GetService<ApplicationDataContext>();
        if (dbContext is null) {
            throw new NullReferenceException(nameof(ApplicationDataContext));
        }
        dbContext.Database.Migrate();
    }
}
