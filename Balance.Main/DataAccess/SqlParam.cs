
namespace Balance.DataAccess;

public class SqlParam
{
    public object Value { get; }

    public SqlParam(object value)
    {
        Value = value;
    }
}
