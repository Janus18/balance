
namespace Balance.DataAccess;

public interface ITransaction : IDisposable
{
    Task Commit();

    Task Rollback();
}
