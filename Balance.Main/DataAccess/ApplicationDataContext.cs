
using Balance.Domain;
using Microsoft.EntityFrameworkCore;

namespace Balance.DataAccess;

public class ApplicationDataContext : DbContext {
    public ApplicationDataContext(DbContextOptions<ApplicationDataContext> options) : base(options)
    {
        this.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Account>(account => {
            account.HasKey(x => x.Id);
            account.Property(x => x.Name).IsRequired();
            account.Property(x => x.Credit).IsRequired();
            account.Property(x => x.Debit).IsRequired();
        });

        modelBuilder.Entity<AccountEntry>(entry => {
            entry.HasKey(x => x.Id);
            entry.Property(x => x.Created).IsRequired();
            entry.Property(x => x.Date).IsRequired();
            entry.Property(x => x.Credit).IsRequired();
            entry.Property(x => x.Debit).IsRequired();
            entry.Property(x => x.PartialCredit).IsRequired();
            entry.Property(x => x.PartialDebit).IsRequired();
            entry.Property(x => x.Notes);
            entry.HasIndex(x => x.Created);
            entry.HasOne(x => x.Account).WithMany().HasForeignKey(x => x.AccountId).IsRequired();
        });
    }
}
