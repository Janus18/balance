
using Balance.Commands;
using Balance.Commands.Results;
using Balance.Domain.Dto;
using Balance.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Balance.Controllers;

[ApiController]
[Route("account/entry")]
public class AccountEntryController : ControllerBase
{
    private readonly IMediator mediator;
    private readonly ILogger<AccountEntryController> logger;

    public AccountEntryController(
        IMediator mediator,
        ILogger<AccountEntryController> logger
    )
    {
        this.mediator = mediator;
        this.logger = logger;
    }

    [HttpPost]
    public async Task<ActionResult<AddEntryResult>> Add(AddEntryDto entry)
    {
        var command = new AddEntryCommand(entry);
        var result = await mediator.Send(command);
        if (result.Success)
        {
            return Created($"/account/entry/{result.Value?.Id}", result.Value);
        }
        else
        {
            return BadRequest(result.Errors);
        }
    }

    [HttpDelete("{entryId}")]
    public async Task<ActionResult> Delete([FromRoute] Guid entryId)
    {
        var command = new RemoveEntryCommand(entryId);
        var result = await mediator.Send(command);
        if (result.Success)
        {
            return NoContent();
        }
        else
        {
            return BadRequest(result.Errors);
        }
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<AccountEntryDto>>> Get(
        [FromQuery] Guid accountId,
        [FromQuery] DateTime from,
        [FromQuery] DateTime to
    )
    {
        var query = new GetEntriesQuery(accountId, from, to);
        var result = await mediator.Send(query);
        return Ok(result);
    }
}
