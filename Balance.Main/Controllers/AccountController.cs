using Balance.Commands;
using Balance.Commands.Results;
using Balance.Domain.Dto;
using Balance.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Balance.Controllers;

[ApiController]
[Route("[controller]")]
public class AccountController : ControllerBase
{
    private readonly IMediator mediator;
    private readonly ILogger<AccountController> _logger;

    public AccountController(
        IMediator mediator,
        ILogger<AccountController> logger
    )
    {
        this.mediator = mediator;
        _logger = logger;
    }

    [HttpPost]
    public async Task<ActionResult<CreateAccountResult>> Create(CreateAccountDto account)
    {
        var command = new CreateAccountCommand(account);
        var result = await mediator.Send(command);
        return Created($"account/{result.Id}", result);
    }

    [HttpPut]
    public async Task<ActionResult> Update(UpdateAccountDto account)
    {
        var command = new UpdateAccountCommand(account);
        var result = await mediator.Send(command);
        if (result.Success)
        {
            return NoContent();
        }
        else
        {
            return BadRequest(result.Errors);
        }
    }

    public async Task<ActionResult<IEnumerable<AccountDto>>> GetAll()
    {
        var query = new GetAccountQuery();
        var result = await mediator.Send(query);
        return Ok(result);
    }
}
