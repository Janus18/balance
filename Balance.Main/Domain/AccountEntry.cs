
namespace Balance.Domain;

public class AccountEntry {
    public Guid Id { get; set; }

    public Account? Account { get; set; }
    public Guid AccountId { get; set; }

    public DateTime Created { get; set; }

    public DateTime Date { get; set; }

    /// <summary>
    /// Debit for the current entry
    /// </summary>
    public decimal Debit { get; set; }

    /// <summary>
    /// Credit for the current entry
    /// </summary>
    public decimal Credit { get; set; }

    /// <summary>
    /// Total debit on the account, up to the current entry
    /// </summary>
    public decimal PartialDebit { get; set; }

    /// <summary>
    /// Total credit on the account, up to the current entry
    /// </summary>
    public decimal PartialCredit { get; set; }

    public string? Notes { get; set; }

    public decimal LocalBalance => Debit - Credit;

    public decimal PartialBalance => PartialDebit - PartialCredit;
}
