
namespace Balance.Domain;

public class Account
{
    public Guid Id { get; set; }

    public string Name { get; set; } = "";

    /// <summary>
    /// Total debit on the account, updated to the last entry
    /// </summary>
    public decimal Debit { get; set; }

    /// <summary>
    /// Total credit on the account, updated to the last entry
    /// </summary>
    public decimal Credit { get; set; }

    public decimal Balance => Debit - Credit;

    public void AddEntry(AccountEntry entry)
    {
        Credit += entry.Credit;
        Debit += entry.Debit;
        entry.PartialCredit = Credit;
        entry.PartialDebit = Debit;
    }

    public void RemoveEntry(AccountEntry entry)
    {
        Credit -= entry.Credit;
        Debit -= entry.Debit;
    }
}
