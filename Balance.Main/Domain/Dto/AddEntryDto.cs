
namespace Balance.Domain.Dto;

public class AddEntryDto
{
    public Guid AccountId { get; set; }

    public DateTime Date { get; set; }

    public decimal Debit { get; set; }

    public decimal Credit { get; set; }

    public string? Notes { get; set; }
}
