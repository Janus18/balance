
namespace Balance.Domain.Dto;

public class AccountDto {
    public Guid Id { get; set; }

    public string Name { get; set; } = "";

    public decimal Debit { get; set; }

    public decimal Credit { get; set; }
}
