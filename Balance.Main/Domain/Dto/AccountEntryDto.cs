
namespace Balance.Domain.Dto;

public class AccountEntryDto {
    public Guid Id { get; set; }

    public DateTime Created { get; set; }

    public DateTime Date { get; set; }

    public decimal Debit { get; set; }

    public decimal Credit { get; set; }

    public string? Notes { get; set; }

    public decimal LocalBalance { get; set; }

    public decimal PartialBalance { get; set; }
}
