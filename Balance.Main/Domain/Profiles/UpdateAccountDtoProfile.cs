
using AutoMapper;

namespace Balance.Domain.Dto;

public class UpdateAccountDtoProfile : Profile {
    public UpdateAccountDtoProfile()
    {
        CreateMap<UpdateAccountDto, Account>()
            .ForMember(x => x.Id, cf => cf.Ignore())
            .ForMember(x => x.Credit, cf => cf.Ignore())
            .ForMember(x => x.Debit, cf => cf.Ignore());
    }
}
