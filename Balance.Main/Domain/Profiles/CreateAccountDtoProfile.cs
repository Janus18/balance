
using AutoMapper;
using Balance.Domain.Dto;

namespace Balance.Domain.Profiles;

public class CreateAccountDtoProfile : Profile {
    public CreateAccountDtoProfile()
    {
        CreateMap<CreateAccountDto, Account>()
            .ForMember(acc => acc.Credit, cf => cf.MapFrom(_ => 0m))
            .ForMember(acc => acc.Debit, cf => cf.MapFrom(_ => 0m))
            .ForMember(acc => acc.Id, cf => cf.Ignore());
    }
}
