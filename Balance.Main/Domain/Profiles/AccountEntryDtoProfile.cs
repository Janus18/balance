
using AutoMapper;
using Balance.Domain.Dto;

namespace Balance.Domain.Profiles;

public class AccountEntryDtoProfile : Profile {
    public AccountEntryDtoProfile()
    {
        CreateMap<AccountEntry, AccountEntryDto>();
    }
}
