
using AutoMapper;
using Balance.Domain.Dto;

namespace Balance.Domain.Profiles;

public class AddEntryProfile : Profile
{
    public AddEntryProfile()
    {
        CreateMap<AddEntryDto, AccountEntry>()
            .ForMember(x => x.Created, cf => cf.MapFrom(_ => DateTime.UtcNow));
    }
}
