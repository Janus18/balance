
using AutoMapper;
using Balance.Domain.Dto;

namespace Balance.Domain.Profiles;

public class AccounDtoProfile : Profile {
    public AccounDtoProfile()
    {
        CreateMap<Account, AccountDto>();
    }
}
