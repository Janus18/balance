
namespace Balance.Models;

public class Error {
    public string Reference { get; }

    public string Description { get; }

    public Error(string reference, string description)
    {
        Reference = reference;
        Description = description;
    }
}
