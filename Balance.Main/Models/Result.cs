
using System.Collections.Immutable;

namespace Balance.Models;

public class Result
{
    public IImmutableList<Error> Errors { get; }

    public bool Success => !Errors.Any();

    protected Result(IImmutableList<Error> errors)
    {
        Errors = errors;
    }

    public static Result Ok()
    {
        return new Result(ImmutableList.Create<Error>());
    }

    public static Result Error(Error err)
    {
        return new Result(ImmutableList.Create<Error>(err));
    }

    public static Result Error(IList<Error> errs)
    {
        return new Result(errs.ToImmutableList());
    }

    public static Result<T> Ok<T>(T value)
    {
        return Result<T>.Ok(value);
    }

    public static Result<T> Error<T>(Error err)
    {
        return Result<T>.Error(new List<Error> { err });
    }

    public static Result<T> Error<T>(IList<Error> errs)
    {
        return Result<T>.Error(errs);
    }
}

public class Result<T> : Result
{
    public T? Value { get; }

    private Result(T value) : base(ImmutableList.Create<Error>())
    {
        Value = value;
    }

    private Result(IImmutableList<Error> errors) : base(errors) { }

    public static Result<T> Ok(T value)
    {
        return new Result<T>(value);
    }

    public new static Result<T> Error(IList<Error> errors)
    {
        return new Result<T>(errors.ToImmutableList());
    }
}
