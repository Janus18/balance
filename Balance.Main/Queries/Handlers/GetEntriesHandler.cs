
using AutoMapper;
using Balance.DataAccess;
using Balance.DataAccess.Order;
using Balance.Domain;
using Balance.Domain.Dto;
using MediatR;

namespace Balance.Queries.Handlers;

public class GetEntriesHandler : IRequestHandler<GetEntriesQuery, IEnumerable<AccountEntryDto>>
{
    private readonly IRepository repository;
    private readonly IMapper mapper;

    public GetEntriesHandler(
        IRepository repository,
        IMapper mapper
    )
    {
        this.repository = repository;
        this.mapper = mapper;
    }

    public async Task<IEnumerable<AccountEntryDto>> Handle(GetEntriesQuery request, CancellationToken cancellationToken)
    {
        var entries = await repository.FindMany(x =>
            x.AccountId == request.AccountId &&
            request.From <= x.Date &&
            x.Date <= request.To,
            Order<AccountEntry>.ByDesc(x => x.Date)
        );
        return mapper.Map<IEnumerable<AccountEntryDto>>(entries);
    }
}
