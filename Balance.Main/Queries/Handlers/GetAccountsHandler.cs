
using AutoMapper;
using Balance.DataAccess;
using Balance.DataAccess.Order;
using Balance.Domain;
using Balance.Domain.Dto;
using MediatR;

namespace Balance.Queries.Handlers;

public class GetAccountsHandler : IRequestHandler<GetAccountQuery, IEnumerable<AccountDto>>
{
    private readonly IRepository repository;
    private readonly IMapper mapper;

    public GetAccountsHandler(
        IRepository repository,
        IMapper mapper
    )
    {
        this.repository = repository;
        this.mapper = mapper;
    }

    public async Task<IEnumerable<AccountDto>> Handle(GetAccountQuery request, CancellationToken cancellationToken)
    {
        var accounts = await repository.FindMany(null, Order<Account>.By(x => x.Name));
        return mapper.Map<IEnumerable<AccountDto>>(accounts);
    }
}
