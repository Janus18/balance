
using Balance.Domain.Dto;
using MediatR;

namespace Balance.Queries;

public class GetAccountQuery : IRequest<IEnumerable<AccountDto>> { }
