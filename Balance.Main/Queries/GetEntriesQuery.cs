
using Balance.Domain.Dto;
using MediatR;

namespace Balance.Queries;

public class GetEntriesQuery : IRequest<IEnumerable<AccountEntryDto>>
{
    public Guid AccountId { get; }

    public DateTime From { get; }

    public DateTime To { get; }

    public GetEntriesQuery(Guid accountId, DateTime from, DateTime to)
    {
        AccountId = accountId;
        From = from;
        To = to;
    }
}
