
using Balance.Commands.Results;
using Balance.Domain.Dto;
using MediatR;

namespace Balance.Commands;

public class CreateAccountCommand : IRequest<CreateAccountResult> {
    public CreateAccountDto Account { get; }

    public CreateAccountCommand(CreateAccountDto account)
    {
        Account = account;
    }
}
