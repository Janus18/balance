
using Balance.Models;
using MediatR;

namespace Balance.Commands;

public class RemoveEntryCommand : IRequest<Result>
{
    public Guid EntryId { get; }

    public RemoveEntryCommand(Guid entryId)
    {
        EntryId = entryId;
    }
}
