
using AutoMapper;
using Balance.DataAccess;
using Balance.Domain;
using Balance.Domain.Dto;
using Balance.Models;
using Balance.Resources;
using MediatR;
using Microsoft.Extensions.Localization;

namespace Balance.Commands.Handlers;

public class UpdateAccountHandler : IRequestHandler<UpdateAccountCommand, Result>
{
    private readonly IRepository repository;
    private readonly IMapper mapper;
    private readonly IStringLocalizer<ValidationStrings> localizer;

    public UpdateAccountHandler(
        IRepository repository,
        IMapper mapper,
        IStringLocalizer<ValidationStrings> localizer
    )
    {
        this.repository = repository;
        this.mapper = mapper;
        this.localizer = localizer;
    }

    public async Task<Result> Handle(UpdateAccountCommand request, CancellationToken cancellationToken)
    {
        var account = await repository.Find<Account>(x => x.Id == request.Account.Id);
        if (account == null) {
            return Result.Error(new Error(nameof(Account), localizer["AccountNotFound"]));
        }

        mapper.Map<UpdateAccountDto, Account>(request.Account, account);
        repository.Update(account);
        await repository.Save();

        return Result.Ok();
    }
}
