
using AutoMapper;
using Balance.Commands.Results;
using Balance.DataAccess;
using Balance.Domain;
using MediatR;

namespace Balance.Commands.Handlers;

public class CreateAccountHandler : IRequestHandler<CreateAccountCommand, CreateAccountResult> {
    private readonly IRepository repo;
    private readonly IMapper mapper;

    public CreateAccountHandler(
        IRepository repo,
        IMapper mapper
    )
    {
        this.repo = repo;
        this.mapper = mapper;
    }

    public async Task<CreateAccountResult> Handle(CreateAccountCommand request, CancellationToken cancellationToken)
    {
        var account = mapper.Map<Account>(request.Account);
        repo.Add(account);
        await repo.Save();
        return new CreateAccountResult(account.Id);
    }
}
