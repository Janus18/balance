
using Balance.DataAccess;
using Balance.Domain;
using Balance.Models;
using Balance.Resources;
using MediatR;
using Microsoft.Extensions.Localization;

namespace Balance.Commands.Handlers;

public class RemoveEntryHandler : IRequestHandler<RemoveEntryCommand, Result>
{
    private readonly IRepository repository;
    private readonly IStringLocalizer<ValidationStrings> localizer;

    public RemoveEntryHandler(
        IRepository repository,
        IStringLocalizer<ValidationStrings> localizer
    )
    {
        this.repository = repository;
        this.localizer = localizer;
    }

    public async Task<Result> Handle(RemoveEntryCommand request, CancellationToken cancellationToken)
    {
        var entry = await repository.Find<AccountEntry>(
            x => x.Id == request.EntryId,
            new[] { nameof(AccountEntry.Account) }
        );

        if (entry == null)
        {
            var error = new Error(nameof(AccountEntry), localizer["EntryNotFound"]);
            return Result.Error(error);
        }
        if (entry.Account == null)
        {
            throw new NullReferenceException($"Entry's Account is null");
        }

        entry.Account.RemoveEntry(entry);
        repository.Delete(entry);
        repository.Update(entry.Account);
        
        using var transaction = repository.StartTransaction();
        await repository.Save();
        await UpdateNextEntries(entry.AccountId, entry.Date, entry.Credit, entry.Debit);
        await transaction.Commit();

        return Result.Ok();
    }

    private async Task UpdateNextEntries(Guid accountId, DateTime from, decimal credit, decimal debit)
    {
        string partialCredit = $"\"{nameof(AccountEntry.PartialCredit)}\"";
        string partialDebit = $"\"{nameof(AccountEntry.PartialDebit)}\"";
        string date = $"\"{nameof(AccountEntry.Date)}\"";
        string accountIdCol = $"\"{nameof(AccountEntry.AccountId)}\"";
        await repository.RawSqlCommand(
            $"UPDATE \"{nameof(AccountEntry)}\" " +
            $"SET " +
            $"  {partialCredit} = {partialCredit} - {{0}}, " +
            $"  {partialDebit} = {partialDebit} - {{1}} " +
            $"WHERE {date} >= {{2}} AND {accountIdCol} = {{3}};",
            new SqlParam(credit),
            new SqlParam(debit),
            new SqlParam(from),
            new SqlParam(accountId)
        );
    }
}
