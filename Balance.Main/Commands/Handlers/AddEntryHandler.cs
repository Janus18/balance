
using AutoMapper;
using Balance.Commands.Results;
using Balance.DataAccess;
using Balance.Domain;
using Balance.Models;
using Balance.Resources;
using MediatR;
using Microsoft.Extensions.Localization;

namespace Balance.Commands.Handlers;

public class AddEntryHandler : IRequestHandler<AddEntryCommand, Result<AddEntryResult>>
{
    private readonly IRepository repository;
    private readonly IMapper mapper;
    private readonly IStringLocalizer<ValidationStrings> localizer;

    public AddEntryHandler(
        IRepository repository,
        IMapper mapper,
        IStringLocalizer<ValidationStrings> localizer
    )
    {
        this.repository = repository;
        this.mapper = mapper;
        this.localizer = localizer;
    }

    public async Task<Result<AddEntryResult>> Handle(AddEntryCommand request, CancellationToken cancellationToken)
    {
        var account = await repository.Find<Account>(a => a.Id == request.Entry.AccountId);
        if (account == null)
        {
            var error = new Error(nameof(Account), localizer["AccountNotFound"]);
            return Result.Error<AddEntryResult>(error);
        }
        var entry = mapper.Map<AccountEntry>(request.Entry);
        account.AddEntry(entry);

        repository.Add(entry);
        repository.Update(account);
        await repository.Save();

        var result = new AddEntryResult(entry.Id);
        return Result.Ok(result);
    }
}
