
using Balance.Domain.Dto;
using Balance.Models;
using MediatR;

namespace Balance.Commands;

public class UpdateAccountCommand : IRequest<Result> {
    public UpdateAccountDto Account { get; }

    public UpdateAccountCommand(UpdateAccountDto account)
    {
        Account = account;
    }
}
