
using Balance.Commands.Results;
using Balance.Domain.Dto;
using Balance.Models;
using MediatR;

namespace Balance.Commands;

public class AddEntryCommand : IRequest<Result<AddEntryResult>>
{
    public AddEntryDto Entry { get; }

    public AddEntryCommand(AddEntryDto entry)
    {
        Entry = entry;
    }
}
