
namespace Balance.Commands.Results;

public class AddEntryResult
{
    public Guid Id { get; }

    public AddEntryResult(Guid id)
    {
        Id = id;
    }
}
