
namespace Balance.Commands.Results;

public class CreateAccountResult {
    public Guid Id { get; }

    public CreateAccountResult(Guid id)
    {
        Id = id;
    }
}
