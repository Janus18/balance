
using Xunit;

namespace Balance.Test.Setup;

[CollectionDefinition(IntegrationCollection.Integration)]
public class IntegrationCollection : ICollectionFixture<Factory> {
    public const string Integration = "Integration";
}
