
using System;
using Microsoft.Extensions.DependencyInjection;

namespace Balance.Test.Setup;

public class Scope : IDisposable
{
    private readonly IServiceScope scope;

    public Scope(IServiceScope scope)
    {
        this.scope = scope;
    }

    public void Dispose()
    {
        scope.Dispose();
    }

    public T GetService<T>()
    {
        return scope.ServiceProvider.GetService<T>()
            ?? throw new NullReferenceException(nameof(T));
    }
}
