
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Balance.DataAccess;
using Balance.Test.Mocking;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Balance.Test.Setup;

public class Factory : WebApplicationFactory<Program>
{
    private readonly Random random;

    public Factory()
    {
        random = new Random();
    }

    public async Task ClearDatabase() {
        using var scope = GetScope();
        var dataContext = scope.GetService<ApplicationDataContext>();
        string[] tables = new[] { "Account", "AccountEntry" };
        string joinedTables = string.Join(',', tables.Select(t => $"\"{t}\""));
        string sql = $"TRUNCATE {joinedTables} CASCADE;";
        await dataContext.Database.ExecuteSqlRawAsync(sql);
    }

    public async Task Seed<T>(T t) where T : class {
        using var scope = GetScope();
        IRepository repo = scope.GetService<IRepository>();
        repo.Add(t);
        await repo.Save();
    }

    public async Task SeedMany<T>(IEnumerable<T> t) where T : class {
        using var scope = GetScope();
        IRepository repo = scope.GetService<IRepository>();
        repo.AddMany(t);
        await repo.Save();
    }

    public BaseMocker GetMocker() {
        return new BaseMocker(random);
    }

    public Scope GetScope() {
        return new Scope(Services.CreateScope());
    }
}
