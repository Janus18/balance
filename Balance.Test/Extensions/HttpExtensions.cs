
using Balance.Test.Results;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Balance.Test.Extensions;

public static class HttpExtensions {
    public static async Task<NoContentResult> Post(this HttpClient client, string uri) {
        var response = await client.PostAsync(uri, null);
        return await NoContentResult.FromResponse(response);
    }

    public static async Task<NoContentResult> Put(this HttpClient client, string uri) {
        var response = await client.PutAsync(uri, null);
        return await NoContentResult.FromResponse(response);
    }

    public static async Task<JsonResult<T>> Post<T>(this HttpClient client, string uri) {
        var response = await client.PostAsync(uri, null);
        return await JsonResult.FromResponse<T>(response);
    }

    public static async Task<JsonResult<T>> Put<T>(this HttpClient client, string uri) {
        var response = await client.PutAsync(uri, null);
        return await JsonResult.FromResponse<T>(response);
    }

    public static async Task<NoContentResult> PostJson(this HttpClient client, string uri, object? body) {
        var content = ToJsonHttpContent(body);
        var response = await client.PostAsync(uri, content);
        return await NoContentResult.FromResponse(response);
    }

    public static async Task<NoContentResult> PutJson(this HttpClient client, string uri, object? body) {
        var content = ToJsonHttpContent(body);
        var response = await client.PutAsync(uri, content);
        return await NoContentResult.FromResponse(response);
    }

    public static async Task<JsonResult<T>> PostJson<T>(this HttpClient client, string uri, object? body) {
        var content = ToJsonHttpContent(body);
        var response = await client.PostAsync(uri, content);
        return await JsonResult.FromResponse<T>(response);
    }

    public static async Task<JsonResult<T>> PutJson<T>(this HttpClient client, string uri, object? body) {
        var content = ToJsonHttpContent(body);
        var response = await client.PutAsync(uri, content);
        return await JsonResult.FromResponse<T>(response);
    }

    public static async Task<NoContentResult> Delete(this HttpClient client, string uri) {
        var response = await client.DeleteAsync(uri);
        return await NoContentResult.FromResponse(response);
    }

    public static async Task<JsonResult<T>> GetJson<T>(this HttpClient client, string uri) {
        var response = await client.GetAsync(uri);
        return await JsonResult.FromResponse<T>(response);
    }

    private static HttpContent ToJsonHttpContent(object? content)
    {
        var serialized = content != null ? JsonConvert.SerializeObject(content) : "";
        return new StringContent(serialized, Encoding.UTF8, "application/json");
    }
}
