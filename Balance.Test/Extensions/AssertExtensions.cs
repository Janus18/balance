
using System;
using Xunit;

namespace Balance.Test.Extensions;

public static class AssertExtensions
{
    public static void CloseTo(DateTime d1, DateTime d2, TimeSpan by)
    {
        var diff = d1 - d2;
        Assert.True(diff.Duration() <= by);
    }

    public static void GreaterThanOrEqualTo(DateTime expected, DateTime d)
    {
        Assert.True(d >= expected);
    }

    public static void LesserThanOrEqualTo(DateTime expected, DateTime d)
    {
        Assert.True(d <= expected);
    }
}
