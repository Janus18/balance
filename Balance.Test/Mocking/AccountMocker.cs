
using Balance.Domain;

namespace Balance.Test.Mocking;

public static class AccountMocker
{
    public static Account GetAccount(
        this BaseMocker mocker,
        decimal? credit = null,
        decimal? debit = null
    )
    {
        return new Account
        {
            Id = System.Guid.NewGuid(),
            Name = mocker.Str(),
            Credit = credit ?? mocker.Decimal(),
            Debit = debit ?? mocker.Decimal()
        };
    }

    public static AccountEntry GetAccountEntry(
        this BaseMocker mocker,
        System.Guid accountId,
        decimal? credit = null,
        decimal? debit = null,
        System.DateTime? date = null
    )
    {
        return new AccountEntry
        {
            Id = System.Guid.NewGuid(),
            AccountId = accountId,
            Created = mocker.Date(),
            Date = date ?? mocker.Date(),
            Credit = credit ?? mocker.Decimal(),
            Debit = debit ?? mocker.Decimal()
        };
    }
}
