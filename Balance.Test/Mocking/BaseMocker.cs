
using System;

namespace Balance.Test.Mocking;

public class BaseMocker
{
    private readonly Random random;

    public BaseMocker(Random random)
    {
        this.random = random;
    }

    public string Str()
    {
        return Guid.NewGuid().ToString();
    }

    public decimal Decimal(decimal cap = 1000000)
    {
        var d = random.NextDouble();
        return (decimal)d * cap;
    }

    public DateTime Date()
    {
        var days = random.Next(-15 * 365, 15 * 365);
        return DateTime.UtcNow.AddDays(days);
    }
}
