
using Balance.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Balance.Test.Results;

public class NoContentResult {
    public HttpStatusCode StatusCode { get; }

    public IEnumerable<Error> Errors { get; }

    protected NoContentResult(HttpStatusCode status, IEnumerable<Error> errors)
    {
        StatusCode = status;
        Errors = errors;
    }

    public static async Task<NoContentResult> FromResponse(HttpResponseMessage response)
    {
        var errors = await GetErrors(response);
        return new NoContentResult(response.StatusCode, errors);
    }

    protected static async Task<IEnumerable<Error>> GetErrors(HttpResponseMessage message) {
        if (message.IsSuccessStatusCode) {
            return new List<Error>();
        }
        var content = await message.Content.ReadAsStringAsync();
        try {
            return JsonConvert.DeserializeObject<IEnumerable<Error>>(content);
        }
        catch (Exception) {
            return new List<Error>();
        }
    }
}
