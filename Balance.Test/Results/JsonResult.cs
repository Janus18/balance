
using Balance.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Balance.Test.Results;

public class JsonResult {
    public static Task<JsonResult<T>> FromResponse<T>(HttpResponseMessage response) {
        return JsonResult<T>.FromResponse<T>(response);
    }
}

public class JsonResult<T> : NoContentResult {
    public T? Data { get; }

    private JsonResult(HttpStatusCode status, T? data, IEnumerable<Error> errors)
        : base(status, errors)
    {
        Data = data;
    }

    public static async Task<JsonResult<T2>> FromResponse<T2>(HttpResponseMessage response) {
        var data = await ReadContent<T2>(response);
        var errors = await GetErrors(response);
        return new JsonResult<T2>(response.StatusCode, data, errors);
    }

    private static async Task<T2?> ReadContent<T2>(HttpResponseMessage response) {
        if (!response.IsSuccessStatusCode) {
            return default(T2);
        }
        try {
            var content = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T2>(content);
        }
        catch (Exception) {
            return default(T2);
        }
    }
}
