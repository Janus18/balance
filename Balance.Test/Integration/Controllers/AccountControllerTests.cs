
using System.Net;
using System.Threading.Tasks;
using Balance.Commands.Results;
using Balance.DataAccess;
using Balance.Domain;
using Balance.Domain.Dto;
using Balance.Test.Extensions;
using Balance.Test.Mocking;
using Balance.Test.Setup;
using Xunit;

namespace Balance.Test.Integration.Controllers;

[Collection(IntegrationCollection.Integration)]
public class AccountControllerTests : IAsyncLifetime
{
    private readonly Factory factory;

    public AccountControllerTests(Factory factory)
    {
        this.factory = factory;
    }

    public async Task DisposeAsync()
    {
        await factory.ClearDatabase();
    }

    public Task InitializeAsync()
    {
        return Task.CompletedTask;
    }

    [Fact]
    public async Task Create_AddsAccountToDatabase_ReturnsCreatedStatus_WithCreateAccountResult()
    {
        var mocker = factory.GetMocker();
        var account = new CreateAccountDto { Name = mocker.Str() };
        var client = factory.CreateClient();
        var result = await client.PostJson<CreateAccountResult>("/account", account);
        Assert.Equal(HttpStatusCode.Created, result.StatusCode);
        Assert.NotNull(result.Data);
        var accountId = result.Data?.Id;
        Assert.NotEqual(System.Guid.Empty, accountId);

        using var scope = factory.GetScope();
        var repo = scope.GetService<IRepository>();
        var accountDb = await repo.Find<Account>(x => x.Id == accountId);
        Assert.NotNull(accountDb);
        Assert.Equal(account.Name, accountDb?.Name);
        Assert.Equal(0m, accountDb?.Credit);
        Assert.Equal(0m, accountDb?.Debit);
    }

    [Theory]
    [InlineData(null)]
    [InlineData("")]
    public async Task Create_WhenNameIsInvalid_ReturnsBadRequestStatus(string name)
    {
        var mocker = factory.GetMocker();
        var account = new CreateAccountDto { Name = name };
        var client = factory.CreateClient();
        var result = await client.PostJson<CreateAccountResult>("/account", account);
        Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
        Assert.NotNull(result.Errors);
        Assert.NotEmpty(result.Errors);
        Assert.Contains(result.Errors, err => err.Reference == nameof(CreateAccountDto.Name));

        using var scope = factory.GetScope();
        var repo = scope.GetService<IRepository>();
        var accountDb = await repo.Find<Account>(_ => true);
        Assert.Null(accountDb);
    }

    [Fact]
    public async Task Create_WhenAccountIsNull_ReturnsBadRequestStatus()
    {
        var mocker = factory.GetMocker();
        var client = factory.CreateClient();
        var result = await client.PostJson<CreateAccountResult>("/account", null);
        Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
        Assert.NotNull(result.Errors);
        Assert.NotEmpty(result.Errors);

        using var scope = factory.GetScope();
        var repo = scope.GetService<IRepository>();
        var accountDb = await repo.Find<Account>(_ => true);
        Assert.Null(accountDb);
    }

    [Fact]
    public async Task Update_UpdatesExistingAccount_ReturnsNoContentStatus()
    {
        var mocker = factory.GetMocker();
        var account = mocker.GetAccount();
        await factory.Seed(account);

        var update = new UpdateAccountDto
        {
            Id = account.Id,
            Name = mocker.Str()
        };

        var client = factory.CreateClient();
        var result = await client.PutJson("/account", update);
        Assert.Equal(HttpStatusCode.NoContent, result.StatusCode);

        using var scope = factory.GetScope();
        var repo = scope.GetService<IRepository>();
        var accountDb = await repo.Find<Account>(x => x.Id == account.Id);
        Assert.NotNull(accountDb);
        Assert.Equal(update.Name, accountDb?.Name);
        Assert.Equal(account.Credit, accountDb?.Credit);
        Assert.Equal(account.Debit, accountDb?.Debit);
    }

    [Fact]
    public async Task Update_WhenAccountDoesntExist_ReturnsBadRequestStatus()
    {
        var mocker = factory.GetMocker();
        var update = new UpdateAccountDto
        {
            Id = System.Guid.NewGuid(),
            Name = mocker.Str()
        };
        var client = factory.CreateClient();
        var result = await client.PutJson("/account", update);
        Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
        Assert.NotNull(result.Errors);
        Assert.NotEmpty(result.Errors);
        Assert.Contains(result.Errors, e => e.Reference == nameof(Account));
    }

    [Theory]
    [InlineData(null)]
    [InlineData("")]
    public async Task Update_WhenNameIsInvalid_ReturnsBadRequestStatus(string name)
    {
        var mocker = factory.GetMocker();
        var account = mocker.GetAccount();
        await factory.Seed(account);

        var update = new UpdateAccountDto
        {
            Id = account.Id,
            Name = name
        };

        var client = factory.CreateClient();
        var result = await client.PutJson("/account", update);
        Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
        Assert.NotNull(result.Errors);
        Assert.NotEmpty(result.Errors);
        Assert.Contains(result.Errors, e => e.Reference == nameof(UpdateAccountDto.Name));
        
        using var scope = factory.GetScope();
        var repo = scope.GetService<IRepository>();
        var accountDb = await repo.Find<Account>(x => x.Id == account.Id);
        Assert.NotNull(accountDb);
        Assert.Equal(account.Name, accountDb?.Name);
    }

    [Fact]
    public async Task Update_WhenAccountIsNull_ReturnsBadRequestStatus()
    {
        var client = factory.CreateClient();
        var result = await client.PutJson("/account", null);
        Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
        Assert.NotNull(result.Errors);
        Assert.NotEmpty(result.Errors);
    }
}
