
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Balance.Commands.Results;
using Balance.DataAccess;
using Balance.Domain;
using Balance.Domain.Dto;
using Balance.Test.Extensions;
using Balance.Test.Mocking;
using Balance.Test.Setup;
using Xunit;

namespace Balance.Test.Integration.Controllers.AccountEntryControllerTests;

[Collection(IntegrationCollection.Integration)]
public class AddTests : IAsyncLifetime
{
    private readonly Factory factory;

    public AddTests(Factory factory)
    {
        this.factory = factory;
    }

    public async Task DisposeAsync()
    {
        await factory.ClearDatabase();
    }

    public Task InitializeAsync()
    {
        return Task.CompletedTask;
    }

    [Fact]
    public async Task Add_AddsEntryInDatabase_ReturnsCreatedStatus_WithAddEntryResult()
    {
        var mocker = factory.GetMocker();
        var account = mocker.GetAccount();
        await factory.Seed(account);
        var entry = new AddEntryDto
        {
            AccountId = account.Id,
            Date = mocker.Date(),
            Debit = mocker.Decimal(),
            Credit = mocker.Decimal(),
            Notes = mocker.Str()
        };

        var client = factory.CreateClient();
        var result = await client.PostJson<AddEntryResult>("/account/entry", entry);
        Assert.Equal(HttpStatusCode.Created, result.StatusCode);
        Assert.NotNull(result.Data);
        var entryId = result.Data?.Id;
        Assert.NotEqual(Guid.Empty, entryId);

        using var scope = factory.GetScope();
        var repo = scope.GetService<IRepository>();
        var entryDb = await repo.Find<AccountEntry>(x => x.Id == entryId);
        Assert.NotNull(entryDb);
        if (entryDb == null) return;
        Assert.Equal(entry.AccountId, entryDb.AccountId);
        AssertExtensions.CloseTo(entryDb.Created, DateTime.UtcNow, new TimeSpan(0, 0, 5));
        AssertExtensions.CloseTo(entry.Date, entryDb.Date, new TimeSpan(0, 0, 1));
        Assert.Equal(entry.Debit, entryDb.Debit);
        Assert.Equal(entry.Credit, entryDb.Credit);
        Assert.Equal(entry.Notes, entryDb.Notes);
    }

    [Fact]
    public async Task Add_ModifiesAccount_ReturnsCreatedStatus_WithAddEntryResult()
    {
        var mocker = factory.GetMocker();
        var account = mocker.GetAccount();
        await factory.Seed(account);
        var entry = new AddEntryDto
        {
            AccountId = account.Id,
            Date = mocker.Date(),
            Debit = mocker.Decimal(),
            Credit = mocker.Decimal()
        };

        var client = factory.CreateClient();
        var result = await client.PostJson<AddEntryResult>("/account/entry", entry);
        Assert.Equal(HttpStatusCode.Created, result.StatusCode);

        using var scope = factory.GetScope();
        var repo = scope.GetService<IRepository>();
        var accountDb = await repo.Find<Account>(x => x.Id == account.Id);
        Assert.NotNull(accountDb);
        Assert.Equal(account.Debit + entry.Debit, accountDb?.Debit);
        Assert.Equal(account.Credit + entry.Credit, accountDb?.Credit);
    }

    [Fact]
    public async Task Add_EntrysPartialTotalsAreSameAsAccounts_ReturnsCreatedStatus_WithAddEntryResult()
    {
        var mocker = factory.GetMocker();
        var account = mocker.GetAccount();
        await factory.Seed(account);
        var entry = new AddEntryDto
        {
            AccountId = account.Id,
            Date = mocker.Date(),
            Debit = mocker.Decimal(),
            Credit = mocker.Decimal()
        };

        var client = factory.CreateClient();
        var result = await client.PostJson<AddEntryResult>("/account/entry", entry);
        Assert.Equal(HttpStatusCode.Created, result.StatusCode);

        using var scope = factory.GetScope();
        var repo = scope.GetService<IRepository>();
        var accountDb = await repo.Find<Account>(x => x.Id == account.Id);
        var entryDb = await repo.Find<AccountEntry>(x => result.Data != null && x.Id == result.Data.Id);
        Assert.NotNull(accountDb);
        Assert.NotNull(entryDb);
        Assert.Equal(accountDb?.Debit, entryDb?.PartialDebit);
        Assert.Equal(accountDb?.Credit, entryDb?.PartialCredit);
    }

    [Theory]
    [InlineData(1, 15, 20)]
    [InlineData(2, 30, 40)]
    [InlineData(5, 75, 100)]
    [InlineData(10, 150, 200)]
    public async Task Add_SetsAccountTotalsCorrectly_AfterAddingSeveralEntries(int times, decimal debit, decimal credit)
    {
        var mocker = factory.GetMocker();
        var account = new Account
        {
            Name = mocker.Str(),
            Credit = 0m,
            Debit = 0m
        };
        await factory.Seed(account);
        
        var entry = new AddEntryDto
        {
            AccountId = account.Id,
            Date = mocker.Date(),
            Debit = 15m,
            Credit = 20m
        };

        foreach (var i in Enumerable.Range(0, times))
        {
            var client = factory.CreateClient();
            var result = await client.PostJson<AddEntryResult>("/account/entry", entry);
            Assert.Equal(HttpStatusCode.Created, result.StatusCode);
        }

        using var scope = factory.GetScope();
        var repo = scope.GetService<IRepository>();
        var accountDb = await repo.Find<Account>(x => x.Id == account.Id);
        Assert.NotNull(accountDb);
        Assert.Equal(debit, accountDb?.Debit);
        Assert.Equal(credit, accountDb?.Credit);
    }

    [Fact]
    public async Task Add_WhenAccountDoesntExist_ReturnsBadRequestStatus()
    {
        var mocker = factory.GetMocker();
        var entry = new AddEntryDto
        {
            AccountId = Guid.NewGuid()
        };

        var client = factory.CreateClient();
        var result = await client.PostJson<AddEntryResult>("/account/entry", entry);
        Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
        Assert.NotNull(result.Errors);
        Assert.Contains(result.Errors, err => err.Reference == nameof(AccountEntry.Account));
    }

    [Fact]
    public async Task Add_WhenEntryIsNull_ReturnsBadRequestStatus()
    {
        var client = factory.CreateClient();
        var result = await client.PostJson<AddEntryResult>("/account/entry", null);
        Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
        Assert.NotNull(result.Errors);
    }    
}
