
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Balance.Domain.Dto;
using Balance.Test.Common;
using Balance.Test.Extensions;
using Balance.Test.Mocking;
using Balance.Test.Setup;
using Xunit;

namespace Balance.Test.Integration.Controllers.AccountEntryControllerTests;

[Collection(IntegrationCollection.Integration)]
public class GetTests : IAsyncLifetime
{
    private readonly Factory factory;

    public GetTests(Factory factory)
    {
        this.factory = factory;
    }

    public async Task DisposeAsync()
    {
        await factory.ClearDatabase();
    }

    public Task InitializeAsync()
    {
        return Task.CompletedTask;
    }

        [Fact]
    public async Task Get_ReturnsOkStatus_WithList_OfAccountEntryDto_DataIsCorrect()
    {
        var mocker = factory.GetMocker();

        var account = mocker.GetAccount();
        await factory.Seed(account);

        var initialDate = new DateTime(2000, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        var entries = Enumerable.Range(0, 10)
            .Select(i => mocker.GetAccountEntry(account.Id, date: initialDate.AddDays(i)))
            .ToList();
        await factory.SeedMany(entries);

        var client = factory.CreateClient();
        var queries = new[] { $"accountId={account.Id}", $"from=2000-01-01T00:00:00Z", $"to=2000-12-31T00:00:00Z" };
        var result = await client.GetJson<IEnumerable<AccountEntryDto>>($"/account/entry?{string.Join('&', queries)}");
        Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        Assert.NotNull(result.Data);
        Assert.True(result.Data?.Count() == 10);

        foreach (var entry in result.Data ?? new List<AccountEntryDto>())
        {
            var entryDb = entries.FirstOrDefault(x => x.Id == entry.Id);
            Assert.NotNull(entryDb);
            if (entryDb == null) return;
            AssertExtensions.CloseTo(entryDb.Created, entry.Created, new TimeSpan(0, 0, 1));
            Assert.Equal(entryDb.Credit, entry.Credit);
            AssertExtensions.CloseTo(entryDb.Date, entry.Date, new TimeSpan(0, 0, 1));
            Assert.Equal(entryDb.Debit, entry.Debit);
            Assert.Equal(entryDb.LocalBalance, entry.LocalBalance);
            Assert.Equal(entryDb.Notes, entry.Notes);
            Assert.Equal(entryDb.PartialBalance, entry.PartialBalance);
        }
    }

    [Fact]
    public async Task Get_FiltersEntriesByAccount_ReturnsOkStatus_WithList_OfAccountEntryDto()
    {
        var mocker = factory.GetMocker();

        var accounts = Enumerable.Range(0, 10).Select(_ => mocker.GetAccount()).ToList();
        await factory.SeedMany(accounts);

        var initialDate = new DateTime(2000, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        var entries = accounts.SelectMany(acc =>
            Enumerable.Range(0, 10).Select(i => mocker.GetAccountEntry(acc.Id, date: initialDate.AddDays(i)))
        ).ToList();
        await factory.SeedMany(entries);

        var client = factory.CreateClient();
        var accountId = accounts[1].Id;
        var queries = new[] { $"accountId={accountId}", $"from=2000-01-01T00:00:00Z", $"to=2000-12-31T00:00:00Z" };
        var result = await client.GetJson<IEnumerable<AccountEntryDto>>($"/account/entry?{string.Join('&', queries)}");
        Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        Assert.NotNull(result.Data);
        Assert.True(result.Data?.Count() == 10);

        var expectedEntries = entries.Where(x => x.AccountId == accountId);
        Assert.All(result.Data, x => Assert.Contains(expectedEntries, y => y.Id == x.Id));
    }

    [Theory]
    [InlineData("2000-01-01T00:00:00.0000000Z", "2000-12-31T00:00:00.0000000Z", 365)]
    [InlineData("2000-01-01T00:00:00.0000000Z", "2000-01-31T00:00:00.0000000Z", 31)]
    [InlineData("2000-06-01T00:00:00.0000000Z", "2000-06-10T00:00:00.0000000Z", 10)]
    [InlineData("2000-10-17T00:00:00.0000000Z", "2000-10-17T12:00:00.0000000Z", 1)]
    [InlineData("2000-05-01T00:00:00.0000000Z", "2000-01-01T00:00:00.0000000Z", 0)]
    public async Task Get_FindsEveryAccountEntryBetweenDates_ReturnsOkStatus_WithList_OfAccountEntryDto(
        string from,
        string to,
        int expected
    )
    {
        var mocker = factory.GetMocker();

        var account = mocker.GetAccount();
        await factory.Seed(account);

        var initialDate = new DateTime(2000, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        var entries = Enumerable.Range(0, 365)
            .Select(i => mocker.GetAccountEntry(account.Id, date: initialDate.AddDays(i)))
            .ToList();
        await factory.SeedMany(entries);

        var client = factory.CreateClient();
        var queries = new[] { $"accountId={account.Id}", $"from={from}", $"to={to}" };
        var result = await client.GetJson<IEnumerable<AccountEntryDto>>($"/account/entry?{string.Join('&', queries)}");
        Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        Assert.NotNull(result.Data);
        Assert.True(result.Data?.Count() == expected);

        var fromDate = DateTimeCommon.ParseExact(from);
        var toDate = DateTimeCommon.ParseExact(to);
        Assert.All(result.Data, x => AssertExtensions.GreaterThanOrEqualTo(fromDate, x.Date));
        Assert.All(result.Data, x => AssertExtensions.LesserThanOrEqualTo(toDate, x.Date));

        var expectedEntries = entries.Where(x => fromDate <= x.Date && x.Date <= toDate);
        Assert.All(result.Data, x => Assert.Contains(expectedEntries, y => y.Id == x.Id));
    }
}
