
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Balance.DataAccess;
using Balance.Domain;
using Balance.Test.Extensions;
using Balance.Test.Mocking;
using Balance.Test.Setup;
using Xunit;

namespace Balance.Test.Integration.Controllers.AccountEntryControllerTests;

[Collection(IntegrationCollection.Integration)]
public class DeleteTests : IAsyncLifetime
{
    private readonly Factory factory;

    public DeleteTests(Factory factory)
    {
        this.factory = factory;
    }

    public async Task DisposeAsync()
    {
        await factory.ClearDatabase();
    }

    public Task InitializeAsync()
    {
        return Task.CompletedTask;
    }

    [Fact]
    public async Task Delete_RemovesEntryFromDatabase_ReturnsNoContentStatus()
    {
        var mocker = factory.GetMocker();
        
        var account = mocker.GetAccount();
        await factory.Seed(account);

        var entries = Enumerable.Range(0, 10).Select(_ => mocker.GetAccountEntry(account.Id)).ToList();
        await factory.SeedMany(entries);

        var entryId = entries[5].Id;
        var client = factory.CreateClient();
        var result = await client.Delete($"/account/entry/{entryId}");
        Assert.Equal(HttpStatusCode.NoContent, result.StatusCode);

        using var scope = factory.GetScope();
        var repo = scope.GetService<IRepository>();
        var dbEntries = await repo.FindMany<AccountEntry>(x => x.AccountId == account.Id);
        Assert.True(dbEntries.Count() == 9);
        Assert.DoesNotContain(dbEntries, x => x.Id == entryId);
    }

    [Fact]
    public async Task Delete_UpdatesAccount_ReturnsNoContentStatus()
    {
        var mocker = factory.GetMocker();
        
        var account = mocker.GetAccount();
        await factory.Seed(account);

        var entry = mocker.GetAccountEntry(account.Id);
        await factory.Seed(entry);

        var client = factory.CreateClient();
        var result = await client.Delete($"/account/entry/{entry.Id}");
        Assert.Equal(HttpStatusCode.NoContent, result.StatusCode);

        using var scope = factory.GetScope();
        var repo = scope.GetService<IRepository>();
        var accountDb = await repo.Find<Account>(x => x.Id == account.Id);
        Assert.Equal(account.Credit - entry.Credit, accountDb?.Credit);
        Assert.Equal(account.Debit - entry.Debit, accountDb?.Debit);
    }

    [Theory]
    [InlineData(1, 990, 975)]
    [InlineData(2, 980, 950)]
    [InlineData(5, 950, 875)]
    [InlineData(10, 900, 750)]
    public async Task Delete_WhenSeveralEntriesAreDeleted_UpdatesAccountWithCorrectTotals(int times, decimal credit, decimal debit)
    {
        var mocker = factory.GetMocker();
        
        var account = mocker.GetAccount(1000m, 1000m);
        await factory.Seed(account);

        var entries = Enumerable.Range(0, 10)
            .Select(_ => mocker.GetAccountEntry(account.Id, 10m, 25m))
            .ToList();
        await factory.SeedMany(entries);

        foreach (var i in Enumerable.Range(0, times))
        {
            var client = factory.CreateClient();
            var result = await client.Delete($"/account/entry/{entries[i].Id}");
            Assert.Equal(HttpStatusCode.NoContent, result.StatusCode);
        }

        using var scope = factory.GetScope();
        var repo = scope.GetService<IRepository>();
        var accountDb = await repo.Find<Account>(x => x.Id == account.Id);
        Assert.Equal(credit, accountDb?.Credit);
        Assert.Equal(debit, accountDb?.Debit);
    }

    [Fact]
    public async Task Delete_UpdatesAllNextEntries_ReturnsNoContentStatus()
    {
        var mocker = factory.GetMocker();
        
        var account = mocker.GetAccount();
        await factory.Seed(account);

        var initDate = new DateTime(2000, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        var entries = new List<AccountEntry>
        {
            mocker.GetAccountEntry(account.Id, date: initDate, credit: 10m, debit: 20m),
            mocker.GetAccountEntry(account.Id, date: initDate.AddDays(1), credit: 5m, debit: 8m),
            mocker.GetAccountEntry(account.Id, date: initDate.AddDays(2), credit: 10m, debit: 4m),
            mocker.GetAccountEntry(account.Id, date: initDate.AddDays(3), credit: 20m, debit: 2m),
            mocker.GetAccountEntry(account.Id, date: initDate.AddDays(4), credit: 5m, debit: 4m),
            mocker.GetAccountEntry(account.Id, date: initDate.AddDays(5), credit: 15m, debit: 20m),
            mocker.GetAccountEntry(account.Id, date: initDate.AddDays(6), credit: 20m, debit: 8m),
            mocker.GetAccountEntry(account.Id, date: initDate.AddDays(7), credit: 10m, debit: 8m),
            mocker.GetAccountEntry(account.Id, date: initDate.AddDays(8), credit: 15m, debit: 20m),
            mocker.GetAccountEntry(account.Id, date: initDate.AddDays(9), credit: 5m, debit: 4m),
        };
        foreach (var entry in entries)
        {
            var allPrevious = entries.Where(x => x.Date < entry.Date);
            entry.PartialCredit = allPrevious.Select(x => x.Credit).DefaultIfEmpty(0m).Sum();
            entry.PartialDebit = allPrevious.Select(x => x.Debit).DefaultIfEmpty(0m).Sum();
        }
        await factory.SeedMany(entries);

        var client = factory.CreateClient();
        var entryId = entries[4].Id;
        var result = await client.Delete($"/account/entry/{entryId}");
        Assert.Equal(HttpStatusCode.NoContent, result.StatusCode);

        using var scope = factory.GetScope();
        var repo = scope.GetService<IRepository>();
        var entriesDb = await repo.FindMany<AccountEntry>(x => x.AccountId == account.Id);
        Assert.Equal(9, entriesDb.Count());

        foreach (var entryDb in entriesDb)
        {
            var entry = entries.FirstOrDefault(x => x.Id == entryDb.Id);
            Assert.NotNull(entry);
            if (entryDb.Date < initDate.AddDays(4))
            {
                Assert.Equal(entry?.PartialCredit, entryDb.PartialCredit);
                Assert.Equal(entry?.PartialDebit, entryDb.PartialDebit);
            }
            else
            {
                Assert.Equal(entry?.PartialCredit - 5m, entryDb.PartialCredit);
                Assert.Equal(entry?.PartialDebit - 4m, entryDb.PartialDebit);
            }
        }
    }

    [Fact]
    public async Task Delete_WhenEntryDoesntExist_ReturnsBadRequestStatus()
    {
        var client = factory.CreateClient();
        var result = await client.Delete($"/account/entry/{Guid.NewGuid()}");
        Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
        Assert.NotNull(result.Errors);
        Assert.Contains(result.Errors, err => err.Reference == nameof(AccountEntry));
    }
}
