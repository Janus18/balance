
using System;
using System.Globalization;

namespace Balance.Test.Common;

public static class DateTimeCommon
{
    public const DateTimeStyles Universal = DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal;

    public static DateTime ParseExact(string s, string format = "O")
    {
        return DateTime.ParseExact(s, format, CultureInfo.InvariantCulture, Universal);
    }

    public static DateTime? TryParseExact(string s, string format = "O")
    {
        if (DateTime.TryParseExact(s, format, CultureInfo.InvariantCulture, Universal, out DateTime t))
        {
            return t;
        }
        return null;
    }
}
